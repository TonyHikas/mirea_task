FROM python:3.10-slim-buster
COPY src /src
RUN mkdir /result/
RUN apt update
RUN apt install -y libpq-dev python-dev gcc
RUN pip install psycopg2