import random
import sys


def generate_list(items_count: int) -> list:
    result = []
    for _ in range(items_count):
        result.append(random.randint(0, 1000))
    return result


def print_list(items_list: list):
    for item in items_list:
        print(item)


if __name__ == '__main__':
    count = int(sys.argv[1])
    result_list = generate_list(count)
    print_list(result_list)
