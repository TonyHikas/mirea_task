import psycopg2
connection = psycopg2.connect(database="postgres", user="postgres", password="pass", host="postgres", port=5432)
cursor = connection.cursor()
cursor.execute(
    "SELECT s.name, s.birthday from grades g "
    "JOIN students s ON s.id = g.student_id "
    "WHERE g.subject='Физика' ORDER BY g.grade DESC;"
)
records = cursor.fetchall()

# результат в виде таблицы
for row in records:
    print(' '.join(str(item) for item in row))
